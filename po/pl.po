# Polish translation for video-trimmer.
# Copyright © 2023 the video-trimmer authors.
# This file is distributed under the same license as the video-trimmer package.
# Piotr Drąg <piotrdrag@gmail.com>, 2023.
# Aviary.pl <community-poland@mozilla.org>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: video-trimmer\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/YaLTeR/video-trimmer/-/"
"issues\n"
"POT-Creation-Date: 2023-09-23 13:06+0000\n"
"PO-Revision-Date: 2023-09-24 14:04+0200\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish <community-poland@mozilla.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. Can't extract filenames from a VariantDict yet.
#. Translators: --output commandline option description.
#: src/application.rs:37
msgid "Output file path"
msgstr "Ścieżka do pliku wyjściowego"

#. Translators: --output commandline option arg description.
#: src/application.rs:39
msgid "PATH"
msgstr "ŚCIEŻKA"

#: src/main.rs:40 src/window.blp:27
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.desktop.in.in:3
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:5
msgid "Video Trimmer"
msgstr "Video Trimmer"

#: src/shortcuts.blp:9
msgctxt "shortcut window"
msgid "General"
msgstr "Ogólne"

#: src/shortcuts.blp:12
msgctxt "shortcut window"
msgid "New window"
msgstr "Nowe okno"

#: src/shortcuts.blp:17
msgctxt "shortcut window"
msgid "Play / Pause"
msgstr "Odtworzenie/wstrzymanie"

#: src/shortcuts.blp:22
msgctxt "shortcut window"
msgid "Trim"
msgstr "Przycięcie"

#: src/shortcuts.blp:27
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Skróty klawiszowe"

#: src/shortcuts.blp:32
msgctxt "shortcut window"
msgid "Close window"
msgstr "Zamknięcie okna"

#: src/shortcuts.blp:37
msgctxt "shortcut window"
msgid "Quit"
msgstr "Zakończenie działania"

#. Translators: Title that is shown if we couldn't load the video track for the opened file (but possibly loaded audio).
#: src/video_preview.blp:17
msgid "Could Not Load Video Track"
msgstr "Nie można wczytać ścieżki wideo"

#. Translators: Description that is shown if we couldn't load the video track for the opened file (but possibly loaded audio).
#: src/video_preview.blp:20
msgid ""
"Maybe the file only has an audio track, or maybe the necessary video codec "
"is missing."
msgstr ""
"Być może plik ma tylko ścieżkę dźwiękową, albo brak wymaganego kodeku wideo."

#. Translators: this is appended to the output video file name.
#. So for example "my video.mp4" will become "my video (trimmed).mp4".
#: src/window.rs:280
msgid " (trimmed)"
msgstr " (przycięte)"

#. Translators: checkbox in output file selection dialog that strips audio from the
#. video file.
#: src/window.rs:305
msgid "Remove audio"
msgstr "Usunięcie dźwięku"

#. Translators: checkbox in output file selection dialog.
#: src/window.rs:310
msgid "Accurate trimming, but slower and may lose quality"
msgstr ""
"Dokładne przycinanie, ale wolniejsze i mogące spowodować obniżenie jakości"

#. Translators: error dialog title.
#: src/window.rs:329 src/window.rs:574 src/window.rs:936
msgid "Error"
msgstr "Błąd"

#. Translators: error dialog text.
#: src/window.rs:332 src/window.rs:577 src/window.rs:939
msgid ""
"Video Trimmer can only operate on local files. Please choose another file."
msgstr ""
"Ten program może działać tylko na lokalnych plikach. Proszę wybrać inny plik."

#. Translators: message dialog text.
#: src/window.rs:434
msgid "Trimming…"
msgstr "Przycinanie…"

#. Translators: text on the toast after trimming was done.
#. The placeholder is the video filename.
#: src/window.rs:458
msgid "{} has been saved"
msgstr "Zapisano plik {}"

#. Translators: text on the button of the toast after
#. trimming was done to show the output file in the file
#. manager.
#: src/window.rs:465
msgid "Show in Files"
msgstr "Wyświetl w menedżerze plików"

#. Translators: error dialog text.
#: src/window.rs:486
msgid "Error trimming video"
msgstr "Błąd podczas przycinania pliku wideo"

#: src/window.rs:495
msgid "Could not communicate with the ffmpeg subprocess"
msgstr "Nie można komunikować się z podprocesem FFmpeg"

#. Translators: error dialog text.
#: src/window.rs:536
msgid "Could not create the ffmpeg subprocess"
msgstr "Nie można utworzyć podprocesu FFmpeg"

#. Translators: shown in the About dialog, put your name here.
#: src/window.rs:756
msgid "translator-credits"
msgstr ""
"Piotr Drąg <piotrdrag@gmail.com>, 2023\n"
"Aviary.pl <community-poland@mozilla.org>, 2023"

#. Translators: link title in the About dialog.
#: src/window.rs:759
msgid "Contribute Translations"
msgstr "Pomóż przy tłumaczeniu"

#: src/window.rs:767
msgid "This release contains a minor visual refresh for GNOME 45."
msgstr "To wydanie nieznacznie odświeża wygląd dla GNOME 45."

#: src/window.rs:768
msgid "Tweaked the visual style for the GNOME 45 release."
msgstr "Poprawiono styl wizualny dla wydania GNOME 45."

#: src/window.rs:769
msgid "Fixed app crashing when ffprobe is missing."
msgstr "Naprawiono awarię programu, kiedy brakuje programu ffprobe."

#: src/window.rs:770
msgid "Updated to the GNOME 45 platform."
msgstr "Zaktualizowano do platformy GNOME 45."

#: src/window.rs:771
msgid "Updated translations."
msgstr "Zaktualizowano tłumaczenia."

#. Translators: file chooser file filter name.
#: src/window.rs:905
msgid "Video files"
msgstr "Pliki wideo"

#. Translators: file chooser dialog title.
#: src/window.rs:914
msgid "Open video"
msgstr "Otwarcie pliku wideo"

#. Translators: Primary menu entry that opens a new window.
#: src/window.blp:8
msgid "_New Window"
msgstr "_Nowe okno"

#. Translators: Primary menu entry that opens the Keyboard Shortcuts dialog.
#: src/window.blp:14
msgid "_Keyboard Shortcuts"
msgstr "_Skróty klawiszowe"

#. Translators: Primary menu entry that opens the About dialog.
#: src/window.blp:20
msgid "_About Video Trimmer"
msgstr "_O programie"

#. Translators: Main menu button tooltip.
#: src/window.blp:50 src/window.blp:106
msgid "Main Menu"
msgstr "Menu główne"

#. Translators: Title text on the window when no files are open.
#: src/window.blp:59
msgid "Trim Videos"
msgstr "Przycinanie plików wideo"

#. Translators: Description text on the window when no files are open.
#: src/window.blp:62
msgid "Drag and drop a video here"
msgstr "Tutaj można przeciągnąć plik wideo"

#. Translators: Open button label when no files are open.
#: src/window.blp:66
msgid "Open Video…"
msgstr "Otwórz plik wideo…"

#. Translators: Open button tooltip.
#: src/window.blp:68
msgid "Select a Video"
msgstr "Wybiera plik wideo"

#. Translators: Trim button.
#: src/window.blp:92
msgid "Trim"
msgstr "Przytnij"

#. Translators: Trim button tooltip.
#: src/window.blp:95
msgid "Trim Video"
msgstr "Przycina plik wideo"

#. Translators: Title that is shown upon video preview loading error.
#: src/window.blp:132
msgid "Video Preview Failed to Load"
msgstr "Wczytanie podglądu pliku wideo się nie powiodło"

#. Translators: Description that is shown upon video preview loading error.
#: src/window.blp:135
msgid ""
"Please enter the start and end timestamps manually.\n"
"\n"
"If you're running Video Trimmer under Flatpak, note that opening files by "
"drag-and-drop may not work."
msgstr ""
"Proszę ręcznie podać czas początkowy i końcowy.\n"
"\n"
"Jeśli ten program jest uruchomiony jako pakiet Flatpak, to otwieranie plików "
"przez ich przeciągnięcie może nie działać."

#. Translators: Label for the entry for the start timestamp.
#: src/window.blp:155
msgid "Start"
msgstr "Początek"

#. Translators: Label for the entry for the end timestamp.
#: src/window.blp:172
msgid "End"
msgstr "Koniec"

#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.desktop.in.in:4
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:6
msgid "Trim videos quickly"
msgstr "Szybkie przycinanie plików wideo"

#. Translators: search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.desktop.in.in:6
msgid "Cut;Movie;Film;Clip;"
msgstr ""
"Przytnij;Przycięcie;Cięcie;Tnij;Potnij;Wytnij;Film;Wideo;Video;Movie;Clip;"
"Klip;"

#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:12
msgid "Ivan Molodetskikh"
msgstr "Ivan Molodetskikh"

#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:18
msgid ""
"Video Trimmer cuts out a fragment of a video given the start and end "
"timestamps. The video is never re-encoded, so the process is very fast and "
"does not reduce the video quality."
msgstr ""
"Video Trimmer wycina fragment pliku wideo po podaniu czasu początkowego "
"i końcowego. Wideo nigdy nie jest kodowane ponownie, więc ten proces jest "
"bardzo szybki i nie zmniejsza jego jakości."

#: data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in:25
msgid "Main Window"
msgstr "Okno główne"
